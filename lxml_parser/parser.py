from lxml import etree


_get_path = lambda path, start, end: ((start, end), tuple(path))


class _XmlParserMeta(type):

    def __new__(cls, name, bases, attrs):
        parser_routes = {}
        for attr_name, attr in attrs.iteritems():
            if hasattr(attr, 'parser_meta'):
                for meta_item in attr.parser_meta:
                    key = _get_path(
                        meta_item['path'],
                        meta_item['start'],
                        meta_item['end']
                    )
                    parser_routes[key] = meta_item
        attrs['_parser_routes'] = parser_routes
        return super(_XmlParserMeta, cls).__new__(cls, name, bases, attrs)


class XmlParser(object):

    __metaclass__ = _XmlParserMeta

    def __init__(self, file_obj):
        self.file_obj = file_obj

    def _iteration(self):
        return etree.iterparse(self.file_obj, events=("start", "end"))

    def _run_node_parser(self, path, parents, event, elem):
        _path = _get_path(path, 'start' == event, 'end' == event)
        if _path in self._parser_routes:
            meta = self._parser_routes[_path]
            return getattr(self, meta['name'])(parents, elem.tag, elem.attrib,
                                               elem.text)

    def parse(self):
        path = []
        parents = []
        for event, elem in self._iteration():

            if event == 'start':
                path.append(elem.tag)
                obj = self._run_node_parser(path, parents[:], event, elem)
                parents.append(obj)
            elif event == 'end':
                self._run_node_parser(path, parents[:], event, elem)
                parents.pop()
                path.pop()

            elem.clear()
            for ancestor in elem.xpath('ancestor-or-self::*'):
                while ancestor.getprevious() is not None:
                    del ancestor.getparent()[0]


def node_parser(path, start=True, end=False):
    def wrapper(func):
        parser_meta = getattr(func, 'parser_meta', [])
        parser_meta.append({
            'path': path,
            'start': start,
            'end': end,
            'name': func.func_name,
        })
        func.parser_meta = parser_meta
        return func
    return wrapper
