from nose.tools import *
from StringIO import StringIO

from parser import XmlParser, node_parser


class XmlTestParser(XmlParser):

    def __init__(self, *args, **kwargs):
        super(XmlTestParser, self).__init__(*args, **kwargs)
        self.data = {}

    @node_parser(('root',))
    def root_parser(self, parents, tag, attrs, text):
        obj = {
            'tag': tag,
            'attrs': attrs,
            'parents': parents,
        }
        self.data[tag] = obj
        return obj

    @node_parser(('root',), start=False, end=True)
    def root_parser_end(self, parents, tag, attrs, text):
        obj = {
            'tag': tag,
            'attrs': attrs,
            'parents': parents,
        }
        self.data[tag + '_end'] = obj

    @node_parser(('root', 'elem1'))
    @node_parser(('root', 'elem2'))
    @node_parser(('root', 'elem2', 'elem3'))
    def elem1_parser(self, parents, tag, attrs, text):
        obj = {
            'tag': tag,
            'attrs': attrs,
            'parents': parents,
        }
        self.data[tag] = obj
        return obj


class Test_XmlParser:

    def setUp(self):

        xml = """
        <root attr1="value1">
            <elem1 attr2="value2"/>
            <elem2 attr3="value3">
                <elem3 attr4="value4" />
            </elem2>
        </root>
        """
        self.file = StringIO(xml)

    @istest
    def root_parser_test(self):

        ROOT_DICT = {'tag': 'root', 'attrs': {'attr1': 'value1'}}

        parser = XmlTestParser(self.file)
        parser.parse()

        assert_in('root', parser.data)
        ok_(parser.data['root']['tag'], 'root')
        ok_(parser.data['root']['attrs'], {'attr1': 'value1'})
        eq_(parser.data['root']['parents'], [])

        assert_in('root_end', parser.data)
        ok_(parser.data['root_end']['tag'], 'root')
        ok_(parser.data['root_end']['attrs'], {'attr1': 'value1'})
        ok_(parser.data['root_end']['parents'], [ROOT_DICT])

        assert_in('elem1', parser.data)
        ok_(parser.data['elem1']['tag'], 'elem1')
        ok_(parser.data['elem1']['attrs'], {'attr2': 'value2'})
        ok_(parser.data['elem1']['parents'], [ROOT_DICT])

        assert_in('elem2', parser.data)
        ok_(parser.data['elem2']['tag'], 'elem2')
        ok_(parser.data['elem2']['attrs'], {'attr3': 'value3'})
        ok_(parser.data['elem2']['parents'], [ROOT_DICT])

        assert_in('elem3', parser.data)
        ok_(parser.data['elem3']['tag'], 'elem3')
        ok_(parser.data['elem3']['attrs'], {'attr4': 'value4'})
        ok_(parser.data['elem3']['parents'],
            [ROOT_DICT, {'tag': 'elem2', 'attrs': {'attr3': 'value3'}}])
