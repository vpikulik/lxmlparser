#!/usr/bin/env python

from distutils.core import setup

setup(
    name='LxmlParser',
    version='0.1',
    description='Xml parser based on lxml',
    author='Vitaly Pikulik',
    author_email='v.pikulik@gmail.com',
    url='ssh://git@bitbucket.org/vpikulik/lxmlparser.git',
    packages=('lxml_parser', ),
    install_requires=('lxml',),
)